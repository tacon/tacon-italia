package dev.tacon.italia;

import java.util.Arrays;

import dev.tacon.annotations.NonNull;

final class Utils {

	static String padLeft(final @NonNull String s, final int length, final char paddingChar) {
		final int sLength = s.length();

		final int diff = length - sLength;
		if (diff < 0) {
			return s.substring(-diff, sLength);
		}
		if (diff == 0) {
			return s;
		}
		if (diff == 1) {
			return paddingChar + s;
		}
		final char[] array = new char[diff];
		Arrays.fill(array, paddingChar);
		return new String(array) + s;
	}

	private Utils() {}
}
