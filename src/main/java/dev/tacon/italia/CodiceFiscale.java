package dev.tacon.italia;

import java.text.Normalizer;
import java.time.LocalDate;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;

/**
 * Class to manage "codice fiscale" (fiscal code)
 */
public class CodiceFiscale {

	/**
	 * Enum for sex
	 */
	public enum Sesso {
		M,
		F
	}

	/**
	 * Enum for validation errors
	 */
	public enum Error {
		FORMATO,
		CARATTERE_CONTROLLO,
		COGNOME,
		NOME,
		DATA_NASCITA,
		SESSO,
		CODICE_CATASTALE
	}

	/**
	 * Regular expression for codice fiscale
	 */
	public static final String REGEX = "[A-Z]{6}[0-9LMNPQRSTUV]{2}[ABCDEHLMPRST]{1}[0-7LMNPQRST]{1}[0-9LMNPQRSTUV]{1}[A-Z]{1}[0-9LMNPQRSTUV]{3}[A-Z]{1}";

	private static final char[] OMOCODICI = new char[] { 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V' };
	private static final char[] MESI = new char[] { 'A', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'P', 'R', 'S', 'T' };
	private static final int[] CONTROLLO_PARI = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
	private static final int[] CONTROLLO_DISPARI = new int[] { 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, -1, -1, -1, -1, -1, -1, -1, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
	private static final int[] POSIZIONE_CARATTERI_NUMERICI = { 6, 7, 9, 10, 12, 13, 14 };

	/**
	 * Constructs and returns an {@code Optional<CodiceFiscale>} based on the given codice fiscale string.
	 *
	 * <p>This method attempts to create a new {@link CodiceFiscale} object from the provided codice fiscale string. If the string is not valid (as determined by the {@code isValid} method), an empty {@code Optional} is returned. Otherwise, a new {@code CodiceFiscale} object is constructed using derived components from the codice fiscale string and is then wrapped in an {@code Optional}.</p>
	 *
	 * @param codiceFiscale The fiscal code string from which a {@code CodiceFiscale} object will be constructed; cannot be null.
	 *
	 * @return An {@code Optional<CodiceFiscale>} containing a constructed {@code CodiceFiscale} object if the provided codice fiscale string is valid, otherwise an empty {@code Optional}.
	 */
	public static Optional<CodiceFiscale> of(final @NonNull String codiceFiscale) {
		final String cfUpper = codiceFiscale.toUpperCase();
		if (!isValid(cfUpper)) {
			return Optional.empty();
		}
		return Optional.of(new CodiceFiscale(cfUpper, getSesso(cfUpper), getCodiceCatastale(cfUpper), getDataNascita(cfUpper)));
	}

	/**
	 * Constructs and returns a {@code CodiceFiscale} object based on the given parameters.
	 *
	 * <p>This method creates a new "codice fiscale" (fiscal code) by concatenating sections derived from the surname, name, birth date, gender, and municipal code. A control character is then computed and added to the end of the generated codice fiscale.</p>
	 *
	 * @param cognome The individual's surname; cannot be null.
	 * @param nome The individual's name; cannot be null.
	 * @param dataNascita The individual's birth date; cannot be null.
	 * @param sesso The individual's gender, either Sesso.M or Sesso.F; cannot be null.
	 * @param codiceCatastale The municipal code; cannot be null.
	 *
	 * @return A {@link CodiceFiscale} object constructed from the provided parameters.
	 */
	public static @NonNull CodiceFiscale of(final @NonNull String cognome, final @NonNull String nome, final @NonNull LocalDate dataNascita, final @NonNull Sesso sesso, final @NonNull String codiceCatastale) {
		final String sezioneCognome = getSezioneCognome(cognome);
		final String sezioneNome = getSezioneNome(nome);
		final String sezioneDataSesso = getSezioneDataSesso(dataNascita, sesso);
		final String tmp = sezioneCognome + sezioneNome + sezioneDataSesso + codiceCatastale;
		return new CodiceFiscale(tmp + getCarattereControllo(tmp), sesso, codiceCatastale, dataNascita);
	}

	/**
	 * Determines if the provided "codice fiscale" (fiscal code) is valid based on a predefined regular expression pattern and the control character.
	 *
	 * <p>This method checks if the provided codice fiscale matches the required format specified by the REGEX constant. Furthermore, it verifies the validity of the control character present in the codice fiscale.</p>
	 *
	 * @param codiceFiscale The fiscal code to check for validity; cannot be null and is expected to be in uppercase.
	 *
	 * @return {@code true} if the codice fiscale matches the required format and has a valid control character; {@code false} otherwise.
	 */
	public static boolean isValid(final @NonNull String codiceFiscale) {
		return codiceFiscale.matches(REGEX) && codiceFiscale.charAt(15) == getCarattereControllo(codiceFiscale.substring(0, 15));
	}

	/**
	 * Determines if the provided "codice fiscale" (fiscal code) is valid based on several criteria.
	 *
	 * <p>This method leverages the {@code validate} method to verify the correctness of the provided codice fiscale against its components such as surname, name, birth date, gender, and municipal code. If the returned error set from {@code validate} is empty, it indicates the codice fiscale is valid.</p>
	 *
	 * @param codiceFiscale the fiscal code to check for validity; cannot be null and will be converted to uppercase for validation.
	 * @param cognome the surname for comparison; can be null if not used for validation.
	 * @param nome the name for comparison; can be null if not used for validation.
	 * @param dataNascita the birth date for comparison; can be null if not used for validation.
	 * @param sesso the gender (Sesso.M or Sesso.F) for comparison; can be null if not used for validation.
	 * @param codiceCatastale the municipal code for comparison; can be null if not used for validation.
	 *
	 * @return {@code true} if the codice fiscale is valid based on the provided criteria; {@code false} otherwise.
	 */
	public static boolean isValid(final @NonNull String codiceFiscale, @Nullable final String cognome, @Nullable final String nome, @Nullable final LocalDate dataNascita, @Nullable final Sesso sesso, final @Nullable String codiceCatastale) {
		return validate(codiceFiscale, cognome, nome, dataNascita, sesso, codiceCatastale).isEmpty();
	}

	/**
	 * Validates the provided "codice fiscale" (fiscal code) against a series of criteria.
	 *
	 * <p>The method checks the format, control character, surname, name, gender, date of birth, and municipal code sections of the codice fiscale. Errors found during the validation are returned as a set of {@code Error} enums.</p>
	 *
	 * @param codiceFiscale the fiscal code to validate; cannot be null and will be converted to uppercase for validation.
	 * @param cognome the surname for comparison; can be null if not used for validation.
	 * @param nome the name for comparison; can be null if not used for validation.
	 * @param dataNascita the birth date for comparison; can be null if not used for validation.
	 * @param sesso the gender (Sesso.M or Sesso.F) for comparison; can be null if not used for validation.
	 * @param codiceCatastale the municipal code for comparison; can be null if not used for validation.
	 *
	 * @return a set of {@link Error} enums that were found during validation. If no errors are found, an empty set is returned.
	 */
	public static Set<Error> validate(final @NonNull String codiceFiscale, @Nullable final String cognome, @Nullable final String nome, @Nullable final LocalDate dataNascita, @Nullable final Sesso sesso, final @Nullable String codiceCatastale) {
		final String cfUpper = codiceFiscale.toUpperCase();
		if (!cfUpper.matches(REGEX)) {
			return EnumSet.of(Error.FORMATO);
		}
		final EnumSet<Error> errori = EnumSet.noneOf(Error.class);
		if (cfUpper.charAt(15) != getCarattereControllo(cfUpper.substring(0, 15))) {
			errori.add(Error.CARATTERE_CONTROLLO);
		}
		if (cognome != null) {
			final String caratteriCognome = cfUpper.substring(0, 3);
			if (!caratteriCognome.equalsIgnoreCase(getSezioneCognome(cognome))) {
				errori.add(Error.COGNOME);
			}
		}
		if (nome != null) {
			final String caratteriNome = cfUpper.substring(3, 6);
			if (!caratteriNome.equalsIgnoreCase(getSezioneNome(nome))) {
				errori.add(Error.NOME);
			}
		}
		if (sesso != null) {
			char carattereSesso;
			carattereSesso = getNumeroOmocodia(cfUpper.charAt(9));
			if (sesso == Sesso.M) {
				if (carattereSesso < '0' || carattereSesso > '3') {
					errori.add(Error.SESSO);
				}
			} else if (sesso == Sesso.F) {
				if (carattereSesso < '4' || carattereSesso > '7') {
					errori.add(Error.SESSO);
				}
			}
		}
		if (dataNascita != null) {
			if (getDataNascita(cfUpper).compareTo(dataNascita) != 0) {
				errori.add(Error.DATA_NASCITA);
			}
		}
		if (codiceCatastale != null) {
			if (!getCodiceCatastale(cfUpper).equals(codiceCatastale.toUpperCase())) {
				errori.add(Error.CODICE_CATASTALE);
			}
		}
		return errori;
	}

	private final String codiceFiscale;
	private final Sesso sesso;
	private final String codiceCatastale;
	private final LocalDate dataNascita;
	private final int omocodie;

	private CodiceFiscale(final String codiceFiscale, final Sesso sesso, final String codiceCatastale, final LocalDate dataNascita) {
		this.codiceFiscale = codiceFiscale;
		this.sesso = sesso;
		this.codiceCatastale = codiceCatastale;
		this.dataNascita = dataNascita;
		this.omocodie = getOmocodie(codiceFiscale);
	}

	/**
	 * Gets the codice fiscale (fiscal code)
	 *
	 * @return codice fiscale
	 */
	public String getCodiceFiscale() {
		return this.codiceFiscale;
	}

	/**
	 * Gets the sex
	 *
	 * @return sex
	 */
	public Sesso getSesso() {
		return this.sesso;
	}

	/**
	 * Gets the codice catastale (municipal code)
	 *
	 * @return codice catastale
	 */
	public String getCodiceCatastale() {
		return this.codiceCatastale;
	}

	/**
	 * Gets the birth date
	 *
	 * @return birth date
	 */
	public LocalDate getDataNascita() {
		return this.dataNascita;
	}

	/**
	 * Checks if there are omocodice
	 */
	public boolean isOmocodia() {
		return this.omocodie != 0;
	}

	/**
	 * Gets the set of omocodie
	 */
	public BitSet getOmocodie() {
		return BitSet.valueOf(new long[] { this.omocodie });
	}

	private static int getOmocodie(final String codiceFiscale) {
		int omocodie = 0;
		for (final int pos : POSIZIONE_CARATTERI_NUMERICI) {
			if (codiceFiscale.charAt(pos) > '9') {
				final int mask = 1 << pos;
				omocodie = omocodie | mask;
			}
		}
		return omocodie;
	}

	private static String getCodiceCatastale(final String codiceFiscale) {
		return codiceFiscale.substring(11, 12) + decodificaOmocodia(codiceFiscale.substring(12, 15));
	}

	private static Sesso getSesso(final String codiceFiscale) {
		final char c = codiceFiscale.toUpperCase().charAt(9);
		if (c >= '0' && c <= '3' || c == 'L' || c == 'M' || c == 'N' || c == 'P') {
			return Sesso.M;
		} else if (c >= '4' && c <= '7' || c >= 'Q' && c <= 'T') {
			return Sesso.F;
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static LocalDate getDataNascita(final String codiceFiscale) {
		int anno = Integer.parseInt(decodificaOmocodia(codiceFiscale.substring(6, 8)));
		final int mese = getMese(codiceFiscale.charAt(8));
		final int giorno = Integer.parseInt(decodificaOmocodia(codiceFiscale.substring(9, 11))) % 40;
		final int annoCorrente = LocalDate.now().getYear();
		final int c = annoCorrente % 100;
		if (anno <= c) {
			anno += annoCorrente - annoCorrente % 100;
		} else {
			anno += annoCorrente - annoCorrente % 100 - 100;
		}
		return LocalDate.of(anno, mese + 1, giorno);
	}

	private static char getNumeroOmocodia(final char c) {
		if (c >= '0' && c <= '9') {
			return c;
		}
		for (int i = 0; i < OMOCODICI.length; i++) {
			if (OMOCODICI[i] == c) {
				return (i + "").charAt(0);
			}
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static String decodificaOmocodia(final @NonNull String source) {
		final char[] result = new char[source.length()];
		for (int i = 0; i < source.length(); i++) {
			result[i] = getNumeroOmocodia(source.charAt(i));
		}
		return new String(result);
	}

	private static int getMese(final char c) {
		for (int i = 0; i < MESI.length; i++) {
			if (MESI[i] == c) {
				return i;
			}
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static String getSezioneCognome(final String cognome) {
		final String normString = Normalizer.normalize(cognome, Normalizer.Form.NFD).toUpperCase();

		final char[] result = new char[3];
		int len = 0;
		int i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isConsonante(c)) {
				result[len++] = c;
			}
			i++;
		}

		i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isVocale(c)) {
				result[len++] = c;
			}
			i++;
		}

		while (len < 3) {
			result[len++] = 'X';
		}

		return new String(result);
	}

	private static String getSezioneNome(final String nome) {
		final String normString = Normalizer.normalize(nome, Normalizer.Form.NFD).toUpperCase();

		final char[] result = new char[4];
		int len = 0;
		int i = 0;
		while (len < 4 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isConsonante(c)) {
				result[len++] = c;
			}
			i++;
		}
		if (len == 4) {
			return new String(new char[] { result[0], result[2], result[3] });
		}

		i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isVocale(c)) {
				result[len++] = c;
			}
			i++;
		}

		while (len < 3) {
			result[len++] = 'X';
		}

		return new String(result, 0, 3);
	}

	private static String getSezioneDataSesso(final LocalDate dataNascita, final Sesso sesso) {
		final String sezioneAnno = Utils.padLeft("" + dataNascita.getYear() % 100, 2, '0');
		final String sezioneMese = "" + MESI[dataNascita.getMonthValue() - 1];
		final String sezioneGiorno = Utils.padLeft("" + (dataNascita.getDayOfMonth() + (sesso == Sesso.M ? 0 : 40)), 2, '0');
		return sezioneAnno + sezioneMese + sezioneGiorno;
	}

	private static char getCarattereControllo(final String codiceFiscale) {
		int somma = 0;
		for (int i = 0; i < 15; i += 2) {
			somma += CONTROLLO_DISPARI[codiceFiscale.charAt(i) - '0'];
		}
		for (int i = 1; i < 15; i += 2) {
			somma += CONTROLLO_PARI[codiceFiscale.charAt(i) - '0'];
		}
		return (char) ('A' + somma % 26);
	}

	private static boolean isConsonante(final char c) {
		return c >= 'A' && c <= 'Z' && !isVocale(c);
	}

	private static boolean isVocale(final char c) {
		return c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
	}
}