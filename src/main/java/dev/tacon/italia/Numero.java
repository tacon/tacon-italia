package dev.tacon.italia;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public final class Numero {

	/** Valore massimo per il quale avviene la converione da cifre a lettere */
	public static final long MAX_VALUE_LONG = 999999999999L;
	public static final BigDecimal MAX_VALUE = new BigDecimal(MAX_VALUE_LONG);

	/** Numeri in lettere fino a 19 */
	private static final String[] NUMBER_IN_WORDS = {
			"zero", "uno", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove", "dieci",
			"undici", "dodici", "tredici", "quattordici", "quindici", "sedici", "diciassette", "diciotto", "diciannove" };

	/** decine in lettere */
	private static final String[] TENS_IN_WORDS = { "", "dieci", "venti", "trenta", "quaranta", "cinquanta", "sessanta", "settanta", "ottanta", "novanta" };

	/**
	 * decine in lettere senza l'ultima vocale, serve quando il numero
	 * successivo è uno oppure otto, in questo caso verrà scritto ad esempio
	 * trentuno e non trentauno
	 */
	private static final String[] TENS_IN_WORDS_VOWEL = { "", "dieci", "vent", "trent", "quarant", "cinquant", "sessant", "settant", "ottant", "novant" };

	/** cento */
	private static final String HOUNDRED = "cento";
	/** Potenze di mille in lettere, se la quantità è uno */
	private static final String[] ONE_THOUSANDS_IN_WORDS = { "uno", "mille", "unmilione", "unmiliardo", "millemiliardi", "unmilionedimiliardi",
			"unmiliardodimiliardi" };
	/** Potenze di mille in lettere, se la quantità è maggiore uno */
	private static final String[] THOUSANDS_IN_WORDS = { "", "mila", "milioni", "miliardi", "milamiliardi", "milionidimiliardi", "miliardidimiliardi" };

	private Numero() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static String inLettere(final long numero) {
		if (numero > MAX_VALUE_LONG || numero < -MAX_VALUE_LONG) {
			throw new IllegalArgumentException("Numero troppo grande (" + numero + ")");
		}
		final StringBuilder sb = new StringBuilder();
		inLettere(sb, numero);
		return sb.toString();
	}

	public static String inLettere(final int numero) {
		// Integer.MAX_VALUE < MAX_VALUE_LONG
		final StringBuilder sb = new StringBuilder();
		inLettere(sb, numero);
		return sb.toString();
	}

	public static String inLettere(final BigDecimal numero, final int decimali) {
		return inLettere(numero, decimali, "/");
	}

	public static String inLettere(final BigDecimal numero, final int decimali, final String separatore) {
		if (numero == null) {
			return "";
		}
		if (numero.abs().compareTo(MAX_VALUE) > 0) {
			throw new IllegalArgumentException("Numero troppo grande(" + numero + ")");
		}
		final StringBuilder sb = new StringBuilder();
		final long longValue = numero.longValue();
		inLettere(sb, longValue);
		if (decimali > 0) {
			final BigDecimal frazione = numero.subtract(new BigDecimal(longValue)).abs();
			final BigDecimal a = frazione.round(new MathContext(decimali, RoundingMode.HALF_EVEN));
			final BigDecimal b = a.movePointRight(decimali);
			final BigInteger dec = b.toBigInteger();
			sb.append(separatore);
			sb.append(Utils.padLeft(dec + "", decimali, '0'));
		}
		return sb.toString();
	}

	private static void inLettere(final StringBuilder sb, final long value) {
		if (value > -NUMBER_IN_WORDS.length && value < NUMBER_IN_WORDS.length) {
			if (value < 0) {
				sb.append('-' + NUMBER_IN_WORDS[(int) -value]);
			} else {
				sb.append(NUMBER_IN_WORDS[(int) value]);
			}
			return;
		}
		long positiveValue = Math.abs(value);
		int gruppoDiTre = 0;
		while (positiveValue > 0) {
			final int tmp = (int) (positiveValue % 1000);

			if (tmp > 1) {
				sb.insert(0, lettere3(tmp) + THOUSANDS_IN_WORDS[gruppoDiTre]);
			} else if (tmp == 1) {
				sb.insert(0, ONE_THOUSANDS_IN_WORDS[gruppoDiTre]);
			}

			positiveValue = positiveValue / 1000;
			gruppoDiTre++;
		}
		if (value < 0) {
			sb.insert(0, '-');
		}
	}

	/** calcola il numero in lettere per numeri inferiore a 1000 */
	private static String lettere3(final int n) {
		if (n == 0) {
			return "";
		}
		String s = "";
		final int centinaia = n / 100;
		if (centinaia > 1) {
			s = NUMBER_IN_WORDS[centinaia];
		}
		if (centinaia > 0) {
			s = s + HOUNDRED;
		}
		s = s + lettere2(n % 100);
		return s;
	}

	/** calcola il numero in lettere per numeri inferiore a 100 */
	private static String lettere2(final int n) {
		if (n == 0) {
			return "";
		}
		if (n < NUMBER_IN_WORDS.length) {
			return NUMBER_IN_WORDS[n];
		}
		final int unita = n % 10;
		if (unita == 1 || unita == 8) {
			return TENS_IN_WORDS_VOWEL[n / 10] + NUMBER_IN_WORDS[unita];
		}
		return TENS_IN_WORDS[n / 10] + (unita > 0 ? NUMBER_IN_WORDS[unita] : "");
	}
}
