package dev.tacon.italia;

/**
 * Class to manage italian vat number
 */
public final class ItalianVatNumber {

	private ItalianVatNumber() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Check if vat number is valid
	 *
	 * @param vatNumber vat number to check
	 */
	public static boolean isValid(final String vatNumber) {
		if (vatNumber.length() != 11) {
			return false;
		}
		// faster than regex, avoid IllegalArgumentException in Luhn.check
		if (!vatNumber.chars().allMatch(i -> i >= '0' && i <= '9')) {
			return false;
		}
		return Luhn.check(vatNumber);
	}
}
